# OcuPay Functions

Hello, this is the repository that stores the functions that are used in the FacePay project.

## Deploy to Your Own AWS Account

### Prerequisites

- [AWS Account](https://aws.amazon.com/)
- [Node.js](https://nodejs.org/en/)
- [AWS CLI](https://aws.amazon.com/cli/)

### Steps

1. Clone this repository. `git clone https://github.com/facepay/facepay-functions.git`

2. Install dependencies. `npm install`

3. Login to your AWS account. `aws configure`

4. Deploy the functions. `npm run deploy`