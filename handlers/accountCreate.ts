import { DynamoDBClient, PutItemCommand } from '@aws-sdk/client-dynamodb'

const client = new DynamoDBClient({ region: 'us-east-1' })

export async function handler (event, _ctx, cb) {
  // Create a customer in Stripe and a user in DynamoDB
  try {
    await client.send(new PutItemCommand({
        TableName: process.env.USERS_TABLE!,
        Item: {
            id: { S: event.userName },
            email: { S: event.request.userAttributes.email },
            cards: { L: [] }
        }
    }))
    cb(null, event)
  } catch (err) {
    cb(err)
  }
}
  