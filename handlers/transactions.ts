import { DynamoDBClient, PutItemCommand, GetItemCommand, UpdateItemCommand } from '@aws-sdk/client-dynamodb'
import * as crypto from 'crypto'

const client = new DynamoDBClient({ region: 'us-east-1' })

export async function onRequest (event, _ctx, cb) {
  const method = event.httpMethod

  const body = JSON.parse(event.body)

  if (method === 'PUT') {
    // Randomly generate a transaction ID in hexadecimals with 16 characters
    const id = crypto.randomBytes(16).toString('hex')

    const putCommand = new PutItemCommand({
      TableName: 'FacePay_Transactions',
      Item: {
        id: { S: id },
        price: { S: String(body.price) },
        description: { S: body.description },
        status: { S: 'PENDING' },
        timeCreated: { S: new Date().toISOString() },
        user: { S: body.userId }
      }
    })

    try {
      await client.send(putCommand)

      cb(null, {
        statusCode: 200,
        body: JSON.stringify({ id })
      })
    } catch (err) {
      cb(err)
    }
  } else if (method === 'GET') {
    const id = event.queryStringParameters.id

    try {
      const { Item } = await client.send(new GetItemCommand({
        TableName: 'FacePay_Transactions',
        Key: { id: { S: id } }
      }))

      cb(null, {
        statusCode: 200,
        body: JSON.stringify(Item)
      })
    } catch (err) {
      cb(err)
    }
  }
}

export async function onCreate(event, _ctx, cb) {
  const { Records } = event

  const transaction = Records[0].dynamodb.NewImage

  if (!transaction) {
    cb(null)
    return
  }

  if (transaction.status.S !== 'PENDING') {
    cb(null)
    return
  }

  await client.send(new UpdateItemCommand({
    TableName: 'FacePay_Transactions',
    Key: { id: { S: transaction.id.S } },
    UpdateExpression: 'SET #status = :status',
    ExpressionAttributeNames: { '#status': 'status' },
    ExpressionAttributeValues: { ':status': { S: 'PROCESSING' } }
  }))

  const userId = transaction.user.S

  let response

  try {
    response = await client.send(new GetItemCommand({
      TableName: 'FacePay_Users',
      Key: { id: { S: userId } }
    }))
  } catch (err) {
    cb(err)
    return
  }

  const user = response.Item

  if (!user) {
    cb(null)
    return
  }

  // Card debiting can happen here, but it is not implemented in this project due to the security concerns

  // Update the transaction status to COMPLETED

  const updateCommand = new UpdateItemCommand({
    TableName: 'FacePay_Transactions',
    Key: { id: { S: transaction.id.S } },
    UpdateExpression: 'SET #status = :status',
    ExpressionAttributeNames: { '#status': 'status' },
    ExpressionAttributeValues: { ':status': { S: 'COMPLETED' } }
  })

  try {
    await client.send(updateCommand)
    cb(null)
  } catch (err) {
    cb(err)
  }
}